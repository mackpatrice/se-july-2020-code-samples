import axios from "axios";

export const getIdFromUrl = (url) => {
  const nextUrl = url.replace("https://pokeapi.co/api/v2/pokemon", "");
  return nextUrl.split("/")[1];
};

// get all pokemon
const getAll = async () => {
  const { data } = await axios.get("https://pokeapi.co/api/v2/pokemon");
  return data.results;
};

// getting one pokemon
const findOne = async (id) => {
  const { data } = await axios.get(`https://pokeapi.co/api/v2/pokemon/${id}`);
  return data;
};

export const pokemonApi = {
  getAll,
  findOne,
};
