import { Container } from "react-bootstrap";
import { Switch, Route } from "react-router-dom";
import { ConnectRedux } from "./classes";
import { UseRedux } from "./functional";
import SwapiExample from "./functional/SWAPI";
import Nav from "./Nav";

function App() {
  return (
    <Container>
      <Nav />
      <Switch>
        <Route path="/" exact>
          <ConnectRedux to="/connect-redux" />
        </Route>
        <Route path="/use-redux">
          <UseRedux />
        </Route>
        <Route path="/swapi">
          <SwapiExample />
        </Route>
        <Route path="*">
          <h1>404 Bruh</h1>
        </Route>
      </Switch>
    </Container>
  );
}

export default App;
