const initialState = 0;

const COUNTER_INCREMENT = "SIMPLE_COUNTER/COUNTER_INCREMENT";
const COUNTER_DECREMENT = "SIMPLE_COUNTER/COUNTER_DECREMENT";
const COUNTER_MULTIPLY = "SIMPLE_COUNTER/COUNTER_MULTIPLY";

const counterReducer = (state = initialState, action) => {
  switch (action.type) {
    case COUNTER_INCREMENT:
      return state + 1;
    case COUNTER_DECREMENT:
      return state - 1;
    case COUNTER_MULTIPLY:
      return state * action.payload;
    default:
      return state;
  }
};

export default counterReducer;

// a pure function
// const add = (a, b) => a + b;

// const multiply = (a, b) => b * a;

// const addBy = num => (a) => num + a;

// const addFiveToNow = num => Date.now() + num;

// const getData = async () => {

//   const res = await fetch("....");
//   const data = await res.json();

//   return data + 5;

// }

// const addByTen = addBy(10);

// addByTen(5);
// addByTen(5);

// add(2,3) --> 5
// add(2,3) --> 5
// add(2,3) --> 5
// add(2,3) --> 5
